package br.com.track.service;

import br.com.track.exception.IntegrationException;
import br.com.track.models.playlist.PlaylistResponse;

public interface PlaylistService {
 
  PlaylistResponse getPlaylist(String accessToken) throws InterruptedException, IntegrationException;
   
}