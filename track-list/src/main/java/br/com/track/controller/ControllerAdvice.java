package br.com.track.controller;

import br.com.track.exception.IntegrationException;
import br.com.track.exception.RestfulError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerAdvice {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(IntegrationException.class)
    public ResponseEntity<RestfulError> exception(IntegrationException exception) {

        RestfulError error = new RestfulError(HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.name(),
                exception.getLocalizedMessage());

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }
}
