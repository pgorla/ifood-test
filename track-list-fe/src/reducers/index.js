import { combineReducers } from 'redux';
import { getPlaylist } from './spotify';
import { getWeather } from './weather';

export default combineReducers({
    getPlaylist,
    getWeather
});
