import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getWeather } from '../actions/weather';

import { Grid, Row, Col, FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap'

class Weather extends Component {

    constructor(props) {
        super(props)

        
    }

    render() {
        return (
            <Col className="horizontal-center" xs={12} sm={12} md={8}>
                <h1 className="title head text-center">Music Fever</h1>
                <form>
                    <FormGroup >
                        <Row>
                            <Col md={6} className="margin-bottom ten">
                                <FormControl
                                    className="search"
                                    id="city"
                                    type="text"
                                    value={this.props.city}
                                    placeholder="Search by city"
                                    onChange={this.props.handleChange}
                                    onFocus={this.props.handleClearCoord}
                                />
                                <FormControl.Feedback />
                            </Col>

                            <Col md={3} className="margin-bottom ten">
                                <FormControl
                                    className="search"
                                    id="lat"
                                    type="text"
                                    value={this.props.lat}
                                    placeholder="Search by lat"
                                    onChange={this.props.handleChange}
                                    onFocus={this.props.handleClearCity}
                                />
                            </Col>

                            <Col md={3} className="margin-bottom ten">
                                <FormControl
                                    className="search"
                                    id="lon"
                                    type="text"
                                    value={this.props.lon}
                                    placeholder="Search by lon"
                                    onChange={this.props.handleChange}
                                    onFocus={this.props.handleClearCity}
                                />
                            </Col>
                        </Row>

                        <Row>
                            <Col md={12} className='text-center'>
                                <Button className='search-button' onClick={() => this.props.handleClick()}>Search</Button>
                            </Col>
                        </Row>
                    </FormGroup>
                </form>
            </Col>
        );
    }
}

export default Weather;
