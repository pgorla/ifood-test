package br.com.track.models.category;

import java.util.List;

public class Category {

    List<CategoryItem> items;

    public List<CategoryItem> getItems() {
        return items;
    }

    public void setItems(List<CategoryItem> items) {
        this.items = items;
    }
}
