package br.com.track.integration;

import br.com.track.models.category.CategoryResponse;
import br.com.track.models.playlist.PlaylistResponse;
import br.com.track.models.track.TrackResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface SpotifyIntegrationService {

    @GET("browse/categories?offset=0&limit=20")
    Call<CategoryResponse> getCategories(@Header("Authorization") String token);

    @GET("browse/categories/{category}/playlists")
    Call<PlaylistResponse> getCategoriesPlaylist(@Header("Authorization") String token, @Path("category") String category);

    @GET("users/{user_id}/playlists/{playlist_id}/tracks")
    Call<TrackResponse> getPlaylistTrack(@Header("Authorization") String token, @Path("user_id") String user, @Path("playlist_id") String playlistId);

}
