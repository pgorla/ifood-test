package br.com.track.integration;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.util.concurrent.TimeUnit;

public abstract class ServiceFactory {

    public static final Integer TIMEOUT = 60;

    private ServiceFactory() {

    }

    /**
     * Creates a retrofit service from an arbitrary class (clazz)
     *
     * @param clazz Java interface of the retrofit service
     * @param url   URL Service
     * @return retrofit service
     */
    public static <T> T createRetrofitService(final Class<T> clazz, final String url) {

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(createOkHttpClient())
                .addConverterFactory(JacksonConverterFactory.create(mapper))
                .build();

        return retrofit.create(clazz);
    }

    /**
     * Creates a OkClient
     *
     * @return client
     */
    public static OkHttpClient createOkHttpClient() {

//        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new Logger() {
//            @Override
//            public void log(String message) {
//                LOG.info(message);
//            }
//        });

//        loggingInterceptor.setLevel(Level.BODY);

        return new OkHttpClient().newBuilder()
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
//                .addInterceptor(loggingInterceptor)
//                .cache(cache)
                .build();

    }

}
