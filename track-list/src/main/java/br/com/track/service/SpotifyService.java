package br.com.track.service;

import br.com.track.exception.IntegrationException;
import br.com.track.models.playlist.PlaylistResponse;
import br.com.track.models.track.TrackResponse;

public interface SpotifyService {

    TrackResponse getTrackByPlaylist(String playlist, String code) throws IntegrationException;

    PlaylistResponse getPlaylistByStyle(String code, String style) throws IntegrationException;

    String authorize() throws IntegrationException;

}
