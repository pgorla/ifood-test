import { ITEMS_FETCH_DATA_SUCCESS, ITEMS_IS_LOADING, ITEMS_HAS_ERRORED, TRACKS_FETCH_DATA_SUCCESS, TRACKS_IS_LOADING } from '../actions/actionTypes'

let initialState = {
    playlist: [],
    hasErrored: false,
    isLoading: false,
    tracks: []
}

export function getPlaylist(state = initialState, action) {
    switch (action.type) {
        case ITEMS_FETCH_DATA_SUCCESS:
            return {
                ...state,
                playlist: action.payload,
                hasErrored: false,
                tracks: []
            };
        case ITEMS_IS_LOADING:
            return {
                ...state,
                playlist: [],
                hasErrored: false,
                isLoading: action.isLoading
            };
        case TRACKS_FETCH_DATA_SUCCESS:
            return {
                ...state,
                hasErrored: false,
                tracks: action.payload
            };
        case TRACKS_IS_LOADING:
            return {
                ...state,
                tracks: [],
                hasErrored: false,
                isLoading: action.isLoading
            };
        case ITEMS_HAS_ERRORED:
            return {
                ...state,
                playlist: [],
                tracks: [],
                hasErrored: action.hasErrored
            };
        default:
            return state;
    }
}