package br.com.track.integration;

import br.com.track.models.weather.Weather;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherIntegrationService {

    @GET("weather")
    Call<Weather> getWeatherByCity(@Query("q") String city, @Query("units") String UNITS, @Query("appid") String APP_ID);

    @GET("weather")
    Call<Weather> getWeatherByCoordinates(@Query("lat") Long lat, @Query("lon") Long lon, @Query("units") String UNITS, @Query("appid") String APP_ID);

}
