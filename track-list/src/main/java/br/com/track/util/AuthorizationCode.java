package br.com.track.util;

import br.com.track.exception.IntegrationException;
import br.com.track.integration.IntegrationConstants;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.credentials.AuthorizationCodeCredentials;
import com.wrapper.spotify.requests.authorization.authorization_code.AuthorizationCodeRefreshRequest;
import com.wrapper.spotify.requests.authorization.authorization_code.AuthorizationCodeRequest;
import com.wrapper.spotify.requests.authorization.authorization_code.AuthorizationCodeUriRequest;

import java.io.IOException;
import java.net.URI;

public class AuthorizationCode {

    private static final SpotifyApi spotifyApi = new SpotifyApi.Builder()
            .setClientId(IntegrationConstants.CLIENT_ID)
            .setClientSecret(IntegrationConstants.CLIENT_SECRET)
            .setRedirectUri(IntegrationConstants.REDIRECT_URI)
            .build();

    private static String authorize(String code) throws IntegrationException {
        try {
            AuthorizationCodeRequest authorizationCodeRequest = spotifyApi.authorizationCode(code).build();

            AuthorizationCodeCredentials authorizationCodeCredentials = authorizationCodeRequest.execute();

            spotifyApi.setAccessToken(authorizationCodeCredentials.getAccessToken());
            spotifyApi.setRefreshToken(authorizationCodeCredentials.getRefreshToken());

            return authorizationCodeCredentials.getAccessToken();

        } catch (IOException | SpotifyWebApiException e) {
            throw new IntegrationException(e);
        }
    }

    private static String refreshToken() throws IntegrationException {
        try {
            AuthorizationCodeRefreshRequest authorizationCodeRefreshRequest = spotifyApi.authorizationCodeRefresh().build();

            AuthorizationCodeCredentials authorizationCodeCredentials = authorizationCodeRefreshRequest.execute();

            spotifyApi.setAccessToken(authorizationCodeCredentials.getAccessToken());

            return authorizationCodeCredentials.getAccessToken();
        } catch (IOException | SpotifyWebApiException e) {
            throw new IntegrationException(e);
        }
    }

    public static String getAccessToken(String code) throws IntegrationException {
        if (spotifyApi.getAccessToken() == null) {
            return authorize(code);
        }

        return refreshToken();
    }

    public static String getAuthenticationUri() {
        AuthorizationCodeUriRequest authorizationCodeUriRequest = spotifyApi.authorizationCodeUri()
                .state("x4xkmn9pu3j6ukrs8n")
                .scope("user-read-birthdate,user-read-email")
                .show_dialog(false)
                .build();

        final URI uri = authorizationCodeUriRequest.execute();

        return uri.toString();
    }

}