import { ITEMS_FETCH_DATA_SUCCESS, ITEMS_IS_LOADING, ITEMS_HAS_ERRORED, TRACKS_FETCH_DATA_SUCCESS, TRACKS_IS_LOADING } from './actionTypes'

import { setHeaders, getStyle } from './fetchUtils'

export function clearTracks() {
    return {
        type: TRACKS_FETCH_DATA_SUCCESS,
        payload: []
    };
}

export function itemsHasErrored(bool) {
    return {
        type: ITEMS_HAS_ERRORED,
        hasErrored: bool
    };
}

export function itemsIsLoading(bool) {
    return {
        type: ITEMS_IS_LOADING,
        isLoading: bool,
    };
}

export function itemsFetchDataSuccess(response) {
    return {
        type: ITEMS_FETCH_DATA_SUCCESS,
        payload: response.items
    };
}

export function tracksIsLoading(bool) {
    return {
        type: TRACKS_IS_LOADING,
        isLoading: bool,
    };
}

export function tracksFetchDataSuccess(response) {
    return {
        type: TRACKS_FETCH_DATA_SUCCESS,
        payload: response.items
    };
}

export function itemsFetchData(code, temp) {
    return (dispatch) => {
        dispatch(itemsIsLoading(true));

        const myInit = setHeaders(code)
        const style = getStyle(temp)

        fetch(`http://localhost:8080/playlists?style=${style}`, myInit)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(itemsIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((items) => {
                if (items.code && items.code !== 200) {
                    dispatch(fetchError())
                } else {
                    dispatch(itemsFetchDataSuccess(items.playlists))
                }
            })
            .catch(() => dispatch(itemsHasErrored(true)));
    };
}

export function trackFetchData(code, playlist) {
    return (dispatch) => {
        dispatch(tracksIsLoading(true));

        const myInit = setHeaders(code)

        fetch(`http://localhost:8080/tracks?playlist=${playlist}`, myInit)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(tracksIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((items) => {
                if (items.code && items.code !== 200) {
                    dispatch(fetchError())
                } else {
                    dispatch(tracksFetchDataSuccess(items))
                }
            })
            .catch(() => dispatch(itemsHasErrored(true)));
    };
}