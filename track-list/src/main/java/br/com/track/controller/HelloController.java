package br.com.track.controller;

import br.com.track.exception.IntegrationException;
import br.com.track.models.playlist.PlaylistResponse;
import br.com.track.models.track.TrackResponse;
import br.com.track.models.weather.Weather;
import br.com.track.service.SpotifyService;
import br.com.track.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class HelloController {

    @Autowired
    SpotifyService spotifyService;

    @Autowired
    WeatherService weatherService;

    @RequestMapping("/weather")
    public ResponseEntity<Weather> getWeather(@RequestParam Map<String, String> requestParams) throws IntegrationException {
        return new ResponseEntity<>(weatherService.getWeatherByCity(requestParams), HttpStatus.OK);
    }

    @RequestMapping("/playlists")
    public ResponseEntity<PlaylistResponse> getPlaylists(@RequestHeader(value = "code") String code,
                                                         @RequestParam(value = "style", defaultValue = "") String style) throws IntegrationException {

        return new ResponseEntity<>(spotifyService.getPlaylistByStyle(code, style), HttpStatus.OK);
    }

    @RequestMapping("/tracks")
    public ResponseEntity<TrackResponse> getTracks(@RequestHeader(value = "code") String code, @RequestParam(value = "playlist") String playlist) throws IntegrationException {


        return new ResponseEntity<>(spotifyService.getTrackByPlaylist(playlist, code), HttpStatus.OK);
    }

    @RequestMapping("/authentication")
    public ResponseEntity<String> getAuthentication() throws IntegrationException {

        return new ResponseEntity<>(spotifyService.authorize(), HttpStatus.OK);
    }

}
