package br.com.track.service;

import br.com.track.exception.IntegrationException;
import br.com.track.models.weather.Weather;

import java.util.Map;

public interface WeatherService {

    Weather getWeatherByCity(Map<String, String> params) throws IntegrationException;

}
