import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { itemsFetchData, trackFetchData, clearTracks } from '../actions/spotify';
import { buildTitle } from '../actions/fetchUtils'

import { Grid, Row, Col, FormGroup, ControlLabel, FormControl, Button, Image } from 'react-bootstrap'

import Tracks from './Tracks'

import Error from './Error'

import '../styles/test.scss'

class Playlist extends Component {

    constructor(props) {
        super(props)

        this.state = {
            playlistId: ''
        }

        this.handleClick = this.handleClick.bind(this)
    }

    handleClick(id) {
        this.setState({ playlistId: id })
    }

    componentWillMount() {
        this.props.getPlaylist(this.props.code, this.props.temp)
    }

    componentWillUpdate(nextProps, nextState) {
        if (this.props.temp != nextProps.temp) {
            this.props.getPlaylist(this.props.code, nextProps.temp)
        } if (this.state.playlistId != nextState.playlistId) {
            this.props.getTracks(this.props.code, nextState.playlistId)
        }
    }

    render() {
        const {
            items,
            isLoading,
            tracks,
            hasErrored
        } = this.props;

        let display = ''
        let back = ''

        if (items.length > 0) {
            display = items.map(item => {
                return <Col key={item.id} xs={6} sm={4} md={3} className="margin-top-30" >
                    <div className='play' onClick={() => this.handleClick(item.id)}>
                        <Image src={item.images[0].url} responsive />
                    </div>
                </Col>
            })
        }

        if (tracks.length > 0) {
            back = <a onClick={this.props.clearTracks} className='pull-left'><i className='back'></i></a>
            display = <Tracks tracks={tracks} />
        }

        if (isLoading) {
            display = <div className="tape"></div>
        }

        if (hasErrored){
            display = <Error />
        }

        return (
            <div>
                <h1 className='text-center title legend'>
                    {back}
                    {buildTitle(this.props.temp)}
                </h1>
                {display}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.getPlaylist.playlist,
        tracks: state.getPlaylist.tracks,
        hasErrored: state.getPlaylist.hasErrored,
        isLoading: state.getPlaylist.isLoading,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getPlaylist: (code, temp) => dispatch(itemsFetchData(code, temp)),
        getTracks: (code, temp) => dispatch(trackFetchData(code, temp)),
        clearTracks: () => dispatch(clearTracks())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Playlist);
