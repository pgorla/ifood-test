package br.com.track.service;

import br.com.track.exception.IntegrationException;
import br.com.track.integration.SpotifyIntegration;
import br.com.track.models.playlist.PlaylistResponse;
import br.com.track.models.track.TrackResponse;
import br.com.track.util.AuthorizationCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SpotifyServiceImpl implements SpotifyService {

    @Autowired
    SpotifyIntegration spotifyIntegration;


    @Override
    public TrackResponse getTrackByPlaylist(String playlist, String code) throws IntegrationException {

        String accessToken = AuthorizationCode.getAccessToken(code);

        return spotifyIntegration.getTracks(playlist, accessToken);
    }

    @Override
    public PlaylistResponse getPlaylistByStyle(String code, String style) throws IntegrationException {
        try {
            String accessToken = AuthorizationCode.getAccessToken(code);

            return StyleEnum.getEnumByWeather(style).getPlaylist(accessToken);
        } catch (InterruptedException e) {
            throw new IntegrationException(e);
        }


    }

    @Override
    public String authorize() throws IntegrationException {
        return AuthorizationCode.getAuthenticationUri();
    }
}
