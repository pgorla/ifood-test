import { GET_WEATHER, ITEMS_IS_LOADING, WEATHER_IS_LOADING, GET_WEATHER_ERROR } from '../actions/actionTypes'

import { setHeaders } from './fetchUtils'

export function itemsIsLoading(bool) {
    return {
        type: WEATHER_IS_LOADING,
        isLoading: bool,
    };
}

export function fetchSuccess(weather) {
    return {
        type: GET_WEATHER,
        payload: weather
    };
}

export function fetchError() {
    return {
        type: GET_WEATHER_ERROR
    };
}

export function getWeather(city, lat, lon) {
    return (dispatch) => {
        dispatch(itemsIsLoading(true));

        let params = []
        city ? params.push(`city=${city}`) : null
        lat ? params.push(`lat=${lat}`) : null
        lon ? params.push(`lon=${lon}`) : null

        const myInit = setHeaders()

        fetch(`http://localhost:8080/weather?${params.join('&')}`, myInit)
            .then((response) => {
                if (!response.ok) {
                }

                dispatch(itemsIsLoading(false));

                return response;
            })
            .catch((error) => dispatch(fetchError()))
            .then((response) => response.json())
            .then((weather) => {
                if (weather.code && weather.code !== 200) {
                    dispatch(fetchError())
                } else {
                    dispatch(fetchSuccess(weather))
                }
            })
    };
}
