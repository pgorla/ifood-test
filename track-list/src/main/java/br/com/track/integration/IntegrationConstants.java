package br.com.track.integration;

import com.wrapper.spotify.SpotifyHttpManager;

import java.net.URI;

public class IntegrationConstants {

    public static final String APP_ID = "b052cbe8f67d3feeadead27770c877f9";
    public static final String UNITS = "metric";
    public static final String WEATHER_BASE_URL = "http://api.openweathermap.org/data/2.5/";
    public static final String SPOTIFY_AUTH_BASE_URL = "https://accounts.spotify.com/authorize/";
    public static final String SPOTIFY_BASE_URL = "https://api.spotify.com/v1/";

    public static final String CLIENT_ID = "e81bf49e9e5a419286653da48f550e4d";
    public static final String CLIENT_SECRET = "c7c7309c5c684dc2b77573a3b1498c45";

    public static final URI REDIRECT_URI = SpotifyHttpManager.makeUri("http://localhost:3000/search");


}
