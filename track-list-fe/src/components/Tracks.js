import React, { Component } from 'react';

import { Grid, Row, Table, Col } from 'react-bootstrap'

import { millisToMinutesAndSeconds } from '../actions/fetchUtils'

import '../styles/test.scss'

class Tracks extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        let display = ''

        if (this.props.tracks.length > 0) {
            display = this.props.tracks.map(item => {
                const artists = item.track.artists.map(artist => {
                    return artist.name
                })
                return <tr>
                    <td>{item.track.name}</td>
                    <td className="hidden-xs">
                        <span>{artists.join(', ')}</span>
                    </td>
                    <td>{millisToMinutesAndSeconds(item.track.duration_ms)}</td>
                </tr>
            })
        }

        return (
            <div>
                <Row>
                    <Col md={12}>
                        <Table responsive hover>
                            <thead>
                                <tr>
                                    <th>Track</th>
                                    <th className="hidden-xs">Artist</th>
                                    <th>Duration</th>
                                </tr>
                            </thead>
                            <tbody>
                                {display}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Tracks;
