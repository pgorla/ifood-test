package br.com.track.integration;

import br.com.track.exception.IntegrationException;
import br.com.track.models.playlist.PlaylistResponse;
import br.com.track.models.track.TrackResponse;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

@Service
public class SpotifyIntegration {

    public PlaylistResponse getPlaylists(String genre, String accessToken) throws IntegrationException {
        try {
            SpotifyIntegrationService spotifyService = ServiceFactory.createRetrofitService(SpotifyIntegrationService.class, IntegrationConstants.SPOTIFY_BASE_URL);

            Call<PlaylistResponse> call = spotifyService.getCategoriesPlaylist("Bearer " + accessToken, genre);

            Response<PlaylistResponse> response = call.execute();


            return APIUtils.handleResponse(response);
        } catch (IOException e) {
            throw new IntegrationException("error trying to get playlists", e);
        }
    }

    public TrackResponse getTracks(String playlist, String accessToken) throws IntegrationException {
        try {
            SpotifyIntegrationService spotifyService = ServiceFactory.createRetrofitService(SpotifyIntegrationService.class, IntegrationConstants.SPOTIFY_BASE_URL);

            Call<TrackResponse> call = spotifyService.getPlaylistTrack("Bearer " + accessToken, "spotify", playlist);

            Response<TrackResponse> response = call.execute();

            return APIUtils.handleResponse(response);
        } catch (IOException e) {
            throw new IntegrationException("error trying to get tracks", e);
        }
    }

}
