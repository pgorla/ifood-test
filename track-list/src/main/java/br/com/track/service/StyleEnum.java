package br.com.track.service;

import br.com.track.exception.IntegrationException;
import br.com.track.integration.SpotifyIntegration;
import br.com.track.models.playlist.PlaylistResponse;

import java.util.HashMap;
import java.util.Map;

public enum StyleEnum implements PlaylistService {

    PARTY("party") {
        @Override
        public PlaylistResponse getPlaylist(String accessToken) throws IntegrationException {
            return spotifyIntegration.getPlaylists(this.getStyle(), accessToken);
        }
    },
    POP("pop") {
        @Override
        public PlaylistResponse getPlaylist(String accessToken) throws IntegrationException {
            return spotifyIntegration.getPlaylists(this.getStyle(), accessToken);
        }
    },
    ROCK("rock") {
        @Override
        public PlaylistResponse getPlaylist(String accessToken) throws IntegrationException {
            return spotifyIntegration.getPlaylists(this.getStyle(), accessToken);
        }
    },
    CLASSICAL("classical") {
        @Override
        public PlaylistResponse getPlaylist(String accessToken) throws IntegrationException {
            return spotifyIntegration.getPlaylists(this.getStyle(), accessToken);
        }
    };

    private static final SpotifyIntegration spotifyIntegration = new SpotifyIntegration();

    private String style;

    public String getStyle() {
        return style;
    }

    StyleEnum(String style) {
        this.style = style;
    }

    private static final Map<String, StyleEnum> ENUM_MAP = new HashMap<>();

    static {
        for (StyleEnum myEnum : values()) {
            ENUM_MAP.put(myEnum.getStyle(), myEnum);
        }
    }

    public static PlaylistService getEnumByWeather(String style) {
        return ENUM_MAP.get(style);
    }

}