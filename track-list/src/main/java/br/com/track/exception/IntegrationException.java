package br.com.track.exception;

public class IntegrationException extends Exception {

    public IntegrationException() {
        super();
    }

    public IntegrationException(Throwable cause) {
        super(cause);
    }

    public IntegrationException(String message) {
        super(message);
    }

    public IntegrationException(String message, Throwable cause) {
        super(message, cause);
    }


}
