package br.com.track.integration;

import br.com.track.exception.IntegrationException;
import br.com.track.models.weather.Weather;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

@Service
public class WeatherIntegration {

    public Weather getWeather(String city) throws IntegrationException {
        try {
            WeatherIntegrationService service = ServiceFactory.createRetrofitService(WeatherIntegrationService.class, IntegrationConstants.WEATHER_BASE_URL);
            Call<Weather> call = service.getWeatherByCity(city, IntegrationConstants.UNITS, IntegrationConstants.APP_ID);

            Response<Weather> response = call.execute();

            return APIUtils.handleResponse(response);
        } catch (IOException e) {
            throw new IntegrationException("error trying to get weather by city", e);
        }
    }

    public Weather getWeatherByCoordinates(Long lat, Long lon) throws IntegrationException {
        try {
            WeatherIntegrationService service = ServiceFactory.createRetrofitService(WeatherIntegrationService.class, IntegrationConstants.WEATHER_BASE_URL);
            Call<Weather> call = service.getWeatherByCoordinates(lat, lon, IntegrationConstants.UNITS, IntegrationConstants.APP_ID);

            Response<Weather> response = call.execute();

            return APIUtils.handleResponse(response);
        } catch (IOException e) {
            throw new IntegrationException("error trying to get weather by coordinates", e);
        }
    }

}
