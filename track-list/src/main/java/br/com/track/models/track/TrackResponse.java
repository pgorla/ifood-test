package br.com.track.models.track;

import java.util.List;

public class TrackResponse {

    private List<Track> items;

    public List<Track> getItems() {
        return items;
    }

    public void setItems(List<Track> items) {
        this.items = items;
    }
}
