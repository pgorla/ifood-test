var path = require("path");

module.exports = {
  type: 'react-app',
  webpack: {
    rules: {
      sass: {
        data: '@import "_variables"',
        includePaths: [path.resolve('src/styles')]
      }
    }
  }
}
