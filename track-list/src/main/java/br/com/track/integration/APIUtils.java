package br.com.track.integration;

import br.com.track.exception.IntegrationException;
import okhttp3.ResponseBody;
import retrofit2.Response;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public abstract class APIUtils {

    private static final String GENERIC_ERROR = "error trying to call service";

    private static final String UNAUTHORIZED_PART_MESSAGE = "Acesso Negado";

    private APIUtils() {

    }

    public static <T> T handleResponse(Response<T> response) throws IntegrationException {

        if (response.isSuccessful()) {

            return response.body();

        } else {
            throw throwError(response.errorBody());

        }
    }

    private static IntegrationException throwError(ResponseBody response) {
        if(response != null){
            try {
                String error = response.string();

                return new IntegrationException(error);
            } catch (IOException e) {
                return new IntegrationException(GENERIC_ERROR, e);
            }


        }

        return new IntegrationException();
    }


}
