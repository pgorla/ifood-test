package br.com.track.models.weather;

public class Weather {

    private String name;
    private MainWeather main;
    private CoordWeather coord;

    public MainWeather getMain() {
        return main;
    }

    public void setMain(MainWeather main) {
        this.main = main;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CoordWeather getCoord() {
        return coord;
    }

    public void setCoord(CoordWeather coord) {
        this.coord = coord;
    }
}
