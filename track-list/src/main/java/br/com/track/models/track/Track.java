package br.com.track.models.track;

public class Track {

    private TrackItem track;

    public TrackItem getTrack() {
        return track;
    }

    public void setTrack(TrackItem track) {
        this.track = track;
    }
}
