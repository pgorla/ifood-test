package br.com.track.service;

import br.com.track.exception.IntegrationException;
import br.com.track.integration.SpotifyIntegration;
import br.com.track.integration.WeatherIntegration;
import br.com.track.models.playlist.PlaylistResponse;
import br.com.track.models.track.TrackResponse;
import br.com.track.models.weather.Weather;
import br.com.track.util.AuthorizationCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class WeatherServiceImpl implements WeatherService {

    @Autowired
    WeatherIntegration weatherIntegration;

    @Override
    public Weather getWeatherByCity(Map<String, String> params) throws IntegrationException {
        if (params.containsKey("city")) {
            return weatherIntegration.getWeather(params.get("city"));
        } else if (params.containsKey("lat") && params.containsKey("lon")) {
            return weatherIntegration.getWeatherByCoordinates(Long.parseLong(params.get("lat")), Long.parseLong(params.get("lon")));
        }

        return null;

    }
}
