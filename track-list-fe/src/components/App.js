import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getWeather } from '../actions/weather';

import { Grid, Row, Col } from 'react-bootstrap'

import Weather from './Weather'

import Playlist from './Playlist'

import Error from './Error'

import axios from 'axios';

class App extends Component {

  constructor() {
    super()

    const params = new URLSearchParams(window.location.search)
    const code = params.get('code')
    if (!code) {
      axios.get('http://localhost:8080/authentication')
        .then(res => {
          window.location.replace(res.data)
        })
    }

    this.state = {
      city: '',
      lat: '',
      lon: ''
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleClearCoord = this.handleClearCoord.bind(this);
    this.handleClearCity = this.handleClearCity.bind(this);
  }

  handleClearCoord(){
    console.log("CLEAR COORD")
    this.setState({lat : ""});
    this.setState({lon : ""});
  }

  handleClearCity(){
    this.setState({city : ""});
  }

  handleChange(e) {
    const key = e.target.id

    let localState = {}
    localState[key] = e.target.value
    this.setState(localState);
  }

  handleClick(e) {
    const {
      weather,
    } = this.props;

    if (!weather) {
      this.props.getWeather(this.state.city, this.state.lat, this.state.lon)
    } else if (this.state.city != '' && weather.name.toLowerCase() != this.state.city.toLowerCase()) {
      this.props.getWeather(this.state.city, '', '')
    } else if (weather.coord.lat != this.state.lat || weather.coord.lon != this.state.lat) {
      this.props.getWeather('', this.state.lat, this.state.lon)
    }
  }

  render() {
    const params = new URLSearchParams(window.location.search)
    const code = params.get('code')

    const {
      weather,
      weatherIsLoading,
      error
    } = this.props;

    let playlistDisplay = ''

    if (weather) {
      const temp = weather.main.temp
      playlistDisplay = <div>
        <Playlist code={code} temp={temp} />
      </div>
    }

    if(weatherIsLoading){
      playlistDisplay = <div className="tape"></div>
    }

    if(error){
      playlistDisplay = <Error />
    }

    return (
      <section>
        <div className="vertical-center">
          <Weather city={this.state.city} lat={this.state.lat} lon={this.state.lon} handleChange={this.handleChange} handleClick={this.handleClick} handleClearCity={this.handleClearCity} handleClearCoord={this.handleClearCoord}/>
        </div>
        <Grid>
          <Row className='margin-bottom thirty'>
            {playlistDisplay}
          </Row>
        </Grid>
      </section>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    weather: state.getWeather.weather,
    weatherIsLoading: state.getWeather.isLoading,
    error: state.getWeather.error
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getWeather: (city, lat, lon) => dispatch(getWeather(city, lat, lon))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
