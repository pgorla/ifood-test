let myHeaders = new Headers();

let myInit = {
    method: 'GET',
    headers: myHeaders,
    mode: 'cors',
    cache: 'default'
};

export const setHeaders = (code) => {
    if (code) {
        myHeaders.append('code', code)
    }
    return myInit
}

export const millisToMinutesAndSeconds = (millis) => {
    var minutes = Math.floor(millis / 60000);
    var seconds = ((millis % 60000) / 1000).toFixed(0);
    return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
}

export const getStyle = (temp) => {
    if (temp > 30) {
        return "party"
    } else if (temp > 14) {
        return "pop"
    } else if (temp > 9) {
        return "rock"
    } else {
        return "classical"
    }
}

export const buildTitle = (temp) => {
    const lets = getStyle(temp)

    return `It's ${temp}º degrees, let's ${lets}`
}