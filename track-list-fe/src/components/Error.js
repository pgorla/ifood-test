import React, { Component } from 'react';

class Error extends Component {

    constructor(props) {
        super(props)
    }

    render() {

        return (
            <div className="text-center">
                <div id="sad">
                    <div className="eye left"></div>
                    <div className="eye right"></div>
                    <div className="mouth"></div>
                </div>
                <h2 className="title error">we have a problem</h2>
            </div>
        );
    }
}

export default Error;
