import { GET_WEATHER, WEATHER_IS_LOADING, GET_WEATHER_ERROR } from '../actions/actionTypes'

let initialState = {
    isLoading: false,
    tracks: [],
    error: false,
    weather: undefined
}

export function getWeather(state = initialState, action) {
    switch (action.type) {
        case GET_WEATHER:
            return {
                ...state,
                weather: action.payload,
                error: false
            };
        case WEATHER_IS_LOADING:
            return {
                ...state,
                weather: undefined,
                isLoading: action.isLoading,
                error: false
            };
        case GET_WEATHER_ERROR:
            return {
                ...state,
                weather: undefined,
                isLoading: false,
                error: true
            };

        default:
            return state;
    }
}