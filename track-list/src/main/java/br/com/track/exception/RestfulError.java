package br.com.track.exception;

import org.springframework.http.HttpStatus;

public class RestfulError {

    private final int code;
    private final String type;
    private final String message;

    public RestfulError(HttpStatus httpStatus, String type) {
        this.code = httpStatus.value();
        this.type = type;
        this.message = httpStatus.getReasonPhrase();
    }

    public RestfulError(HttpStatus httpStatus, String type, String message) {
        this.code = httpStatus.value();
        this.type = type;
        this.message = message;
    }

    public RestfulError(int code, String type, String message) {
        this.code = code;
        this.type = type;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }
}
