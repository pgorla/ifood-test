package br.com.track.models.playlist;

public class PlaylistResponse {

    private Playlist playlists;

    public Playlist getPlaylists() {
        return playlists;
    }

    public void setPlaylists(Playlist playlists) {
        this.playlists = playlists;
    }
}
