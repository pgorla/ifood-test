package br.com.track.models.weather;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MainWeather {

    private String temp;
    private String pressure;
    private String humidity;
    private String tempMin;
    private String tempMax;

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getTempMin() {
        return tempMin;
    }

    @JsonProperty("temp_min")
    public void setTempMin(String tempMin) {
        this.tempMin = tempMin;
    }

    public String getTempMax() {
        return tempMax;
    }

    @JsonProperty("temp_max")
    public void setTempMax(String tempMax) {
        this.tempMax = tempMax;
    }
}
