package br.com.track.models.track;

import br.com.track.models.Artist;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class TrackItem {

    private String name;
    private Long duration;
    private List<Artist> artists;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Artist> getArtists() {
        return artists;
    }

    public void setArtists(List<Artist> artists) {
        this.artists = artists;
    }

    public Long getDuration() {
        return duration;
    }

    @JsonProperty("duration_ms")
    public void setDuration(Long duration) {
        this.duration = duration;
    }
}
