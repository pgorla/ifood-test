package br.com.track.models.playlist;

import java.util.List;

public class Playlist {

    private List<PlaylistItem> items;

    public List<PlaylistItem> getItems() {
        return items;
    }

    public void setItems(List<PlaylistItem> items) {
        this.items = items;
    }
}
